# cloud-init-playground

- Tested on a debian bookworm host 

### `debootstrap`

```bash
cd debootstrap
./bin/install.sh     # install dependencies
./bin/build.sh       # install bookworm w/ cloud-init 23.3
./bin/launch.sh  # run new vm via qemu
# ctrl+a, x to exit qemu
```

### `qemu-cloud-init`

```bash
cd qemu-cloud-init
./bin/install.sh     # install dependencies
./bin/fetch.sh       # download and modifiy cloud image for remotecloud
./bin/imds.sh        # run imds server (will hold open in terminal)
./bin/launch.sh      # run new vm via qemu
# ctrl+a, x to exit qemu
```

## Useful commands

### Write a raw image to a drive

```bash
lsblk
pv img/image.raw | sudo dd of=/dev/XXXX
```

### Make a drive fail to boot
```bash
sudo dd if=/dev/zero bs=2048 count=10 of=/dev/XXXX
```

### Example use:

Install rescue, kill boot drive:

```bash
pv debian-bookworm-remotecloud.raw | sudo tee /dev/disk/by-id/ata-*S2RANXAH385797F > /dev/null
sudo wipefs --all --force /dev/disk/by-id/ata-*S3Z1NB0KC27545F
sudo sgdisk /dev/disk/by-id/ata-*S3Z1NB0KC27545F \
	--clear --zap-all --print
sudo systemctl reboot
```

Install boot drive

```bash
pv debian-bookworm-remotecloud.raw | sudo tee /dev/disk/by-id/ata-*S3Z1NB0KC27545F > /dev/null
sudo systemctl reboot
```
