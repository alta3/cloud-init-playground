# `qemu-cloud-init`

```bash
cd qemu-cloud-init
./bin/install.sh     # install dependencies
./bin/fetch.sh       # download and modifiy cloud image for remotecloud
./bin/imds.sh        # run imds server (will hold open in terminal)
./bin/launch.sh      # run new vm via qemu
```

### Notes:

- `fetch.sh` will need `HOST_IP` updated to the hosting systems' IP address

### References

- https://cloudinit.readthedocs.io/en/22.4.2/topics/datasources/nocloud.html#nocloud
- https://libguestfs.org/guestfish.1.html#from-shell-scripts
