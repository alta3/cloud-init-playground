#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

sudo apt update
sudo apt install -y \
	qemu-system-x86 \
	cloud-init 
