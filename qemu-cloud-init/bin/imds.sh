#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

python3 -m http.server --directory ../imds
