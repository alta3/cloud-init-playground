#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

rm -rf vm/*
rm -rf img/*
