#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

## update HOST_IP
#HOST_IP=192.168.1.100
HOST_IP=192.168.32.164
RELEASE=bookworm
FILE_NAME="debian-12-generic-amd64-daily.raw"
IMAGES_URL="https://cloud.debian.org/images/cloud/${RELEASE}/daily/latest"
IMG_URL="${IMAGES_URL}/${FILE_NAME}"
SHA_URL="${IMAGES_URL}/SHA512SUMS"
CLOUD_NAME=remotecloud

mkdir -p ../img
pushd ../img
curl -C - -L ${IMG_URL} -o ${FILE_NAME}
curl -L ${SHA_URL} | grep ${FILE_NAME} | tee ${FILE_NAME}.sha512
sha512sum -c ${FILE_NAME}.sha512

cp ${FILE_NAME} ${CLOUD_NAME}-${FILE_NAME}
guestfish \
	--rw \
	--add ${CLOUD_NAME}-${FILE_NAME} \
	--mount /dev/sda1:/ << EOF
download /boot/grub/grub.cfg grub.cfg
! sed -i '/^\s*linux\s*.*vmlinuz/ s/$/ ds=nocloud-net\\\;s=http:\/\/${HOST_IP}:8000\//' grub.cfg
upload grub.cfg /boot/grub/grub.cfg
exit
EOF
popd
