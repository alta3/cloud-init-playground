#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

IMG_DIR=${PWD}/../img
SRC_IMAGE=${IMG_DIR}/remotecloud-debian-12-generic-amd64-daily.raw
VM_ID=$(cat /proc/sys/kernel/random/uuid | cut -d "-" -f 1)
VM_DIR=${PWD}/../vm
VM_IMAGE=${VM_DIR}/${VM_ID}.qcow2

mkdir -p vm
qemu-img convert \
	-f raw \
	-O qcow2 \
	${SRC_IMAGE} \
	${VM_IMAGE}

qemu-img resize ${VM_IMAGE} 16G

cloud-init schema --config-file ../imds/user-data --annotate

sudo qemu-system-x86_64 \
	-net nic \
	-net user \
	-machine accel=kvm:tcg \
	-enable-kvm \
	-cpu host \
	-m 2048 \
	-nographic \
	-chardev stdio,id=char0,mux=on,logfile=${VM_DIR}/${VM_ID}.serial.log,signal=off \
	-serial chardev:char0 -mon chardev=char0 \
	-drive file=${VM_IMAGE}
	#-smbios type=1,serial=ds='nocloud-net;s=http://192.168.32.157:8000/'
	#-drive driver=raw,file=vm/seed.iso,if=virtio
