#!/bin/bash
set -euxo pipefail

# Install figlet for readability
sudo apt-get install -y figlet

# Define variables
HOSTNAME="router"
NAMESERVER="192.168.200.1"  # Replace with the desired nameserver IP address
ETHERNET_INTERFACE="enp2s0"
IP_ADDRESS="192.168.200.31/24"
GATEWAY="192.168.200.1"
IMAGE_NAME="efi_image.img"
CLOUD_INIT_USER="ubuntu"

# Specify a Debian mirror to use
MIRROR="http://ftp.us.debian.org/debian/"

figlet "Creating Disk Image"
# Create an empty disk image
dd if=/dev/zero of=$IMAGE_NAME bs=1M count=3072  # 3GB image size to accommodate the root filesystem and EFI partition

figlet "Partitioning Disk Image"
# Create a GPT partition table and two partitions: EFI and root
parted $IMAGE_NAME --script -- mklabel gpt
parted $IMAGE_NAME --script -- mkpart ESP fat32 1M 512M
parted $IMAGE_NAME --script -- mkpart primary ext4 512M 100%

# Set the boot flag on the EFI partition
parted $IMAGE_NAME --script -- set 1 boot on

figlet "Associating Image with Loop Device"
# Associate the image with a loop device
LOOPDEV=$(sudo losetup -fP --show $IMAGE_NAME)

figlet "Formatting Partitions"
# Format the partitions
sudo mkfs.vfat -F 32 "${LOOPDEV}p1"
sudo mkfs.ext4 "${LOOPDEV}p2"

figlet "Mounting Partitions"
# Mount the partitions
EFI_MOUNT="$HOME/efi-mount"
ROOT_MOUNT="$HOME/root-mount"
sudo mkdir -p "$EFI_MOUNT"
sudo mount "${LOOPDEV}p1" "$EFI_MOUNT"

sudo mkdir -p "$ROOT_MOUNT"
sudo mount "${LOOPDEV}p2" "$ROOT_MOUNT"

figlet "Installing Debian Base System"
# Install the base Debian system using debootstrap
sudo debootstrap --arch=amd64 bookworm "$ROOT_MOUNT" "$MIRROR"

figlet "Mounting System Directories"
# Ensure /boot/efi exists and mount the EFI partition inside chroot
sudo mkdir -p "$ROOT_MOUNT/boot/efi"
sudo mount --bind /dev "$ROOT_MOUNT/dev"
sudo mount --bind /proc "$ROOT_MOUNT/proc"
sudo mount --bind /sys "$ROOT_MOUNT/sys"
sudo mount "${LOOPDEV}p1" "$ROOT_MOUNT/boot/efi"

figlet "Installing Required Packages"
# Update package list and install required packages with apt
sudo chroot "$ROOT_MOUNT" apt-get update
sudo chroot "$ROOT_MOUNT" apt-get install -y sudo vim curl openssh-server git tmux qemu-utils gdisk jq pv  linux-image-amd64 netplan.io cloud-init grub-efi-amd64

figlet "Configuring Initramfs"
# Ensure the initramfs is generated
sudo chroot "$ROOT_MOUNT" update-initramfs -u

figlet "Setting Root Password"
# Set the root password 
sudo chroot "$ROOT_MOUNT" bash -c "echo 'root:arg' | chpasswd"

figlet "Creating New User"
# Create a new user with sudo privileges
sudo chroot "$ROOT_MOUNT" bash -c "useradd -m -s /bin/bash $CLOUD_INIT_USER"
sudo chroot "$ROOT_MOUNT" bash -c "echo '$CLOUD_INIT_USER:arg' | chpasswd"
sudo chroot "$ROOT_MOUNT" bash -c "adduser $CLOUD_INIT_USER sudo"

figlet "Configuring Network and DNS"
# No network manager is running here, so DNS must be set manually
sudo tee "$ROOT_MOUNT/etc/resolv.conf" > /dev/null <<EOF
nameserver $NAMESERVER
options edns0 trust-ad
search alta3.com
EOF

# Disable/Remove Language Warning
sudo mkdir -p "$ROOT_MOUNT/var/lib/cloud/instance/"
sudo tee "$ROOT_MOUNT/var/lib/cloud/instance/locale-check.skip" > /dev/null << EOF
# Disablishing Language Warning
EOF

FIGLET "disable language"




# Network configuration via Netplan
sudo tee "$ROOT_MOUNT/etc/netplan/01-netcfg.yaml" > /dev/null <<EOF
network:
  version: 2
  ethernets:
    $ETHERNET_INTERFACE:
      addresses:
        - $IP_ADDRESS
      nameservers:
        addresses:
          - $GATEWAY
      routes:
        - to: 0.0.0.0/0
          via: $GATEWAY
EOF

# Setting permissions for the netplan configuration file
sudo chmod 600 "$ROOT_MOUNT/etc/netplan/01-netcfg.yaml"

figlet "Installing GRUB"
# Install GRUB to the EFI partition
sudo chroot "$ROOT_MOUNT" grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable --recheck --no-floppy

figlet "Configuring GRUB"
# Dynamically detect the kernel and initramfs versions and configure GRUB with /dev/sdb2
KERNEL_VERSION=$(basename "$ROOT_MOUNT/boot/vmlinuz-"* | sed 's/vmlinuz-//')
INITRD_VERSION=$(basename "$ROOT_MOUNT/boot/initrd.img-"* | sed 's/initrd.img-//')

sudo tee "$ROOT_MOUNT/boot/grub/grub.cfg" > /dev/null <<EOF
set timeout=5
set default=0

menuentry "Alta3 Custom Debian" {
    set root=(hd0,gpt2)
    linux /boot/vmlinuz-$KERNEL_VERSION root=/dev/sdb2 ro
    initrd /boot/initrd.img-$INITRD_VERSION
}
EOF

figlet "Configuring Fstab"
# Hard code /etc/fstab with /dev/sdb2
sudo tee "$ROOT_MOUNT/etc/fstab" > /dev/null <<EOF
/dev/sdb2 / ext4 errors=remount-ro 0 1
EOF

figlet "Configuring Cloud-Init"
# Configure cloud-init
sudo tee "$ROOT_MOUNT/etc/cloud/cloud.cfg" > /dev/null <<EOF
#cloud-config

# Specify the datasource
datasource_list: [ None ]

disable_root: false

hostname: $HOSTNAME

users:
  - name: $CLOUD_INIT_USER
    lock_passwd: false
    gecos: Ubuntu
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    shell: /bin/bash
    ssh_authorized_keys:
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSSpuNstkhwIjQlChmVymOnwJ9gBKwV/D4odIDoz3UiFoHYcq5uoz5jk9RyUQAirT0jmtDrPgiBzw7cKk54q8h1x40uvH+GqChcqOXYMUsPnIONOYYOxn/R6mqvzsmtlMs5865f4KgFzNmjyN9YJoNh3x3mELnCBSDBqWyc+nKSBmFBQmBW/mPA9yGp9zQ90AnznjyjqKS9WQlRO/9Bv/iJLoR8AAOgC5WdxXZu6Vm8IVZIhPu3w+cuumPuYhu9p2xDJrQERyk1QOVKbNF1JtfAUQYQNOnq4ZiC8NW+f4kvY/ugqreoHI9zsUP0ddTQk7WtfOSe/TWjYMXkCc6TunKmr4Sq9Qh8cU7laq1fBaGoRgu+tzqSsZ0eyqP/Y/sCcDlfVPPbU9otI5rGzkVjrXLbO9OuH4bKQFgnyDV2wYZbiGyipXor87YLN5U5KwJxNz1zNAbJ3bGfYZQ5eLXXBjgl2ErLaseBlmZspYo49Ogn0AHosTuQ4b+sJoVU6WpUms=
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCZZ+/2c4E0iIykECZwlEeaDraUzw3eSzlnICfXcx4rxMaKGCQu4FoEq6b7DToUvJxWGATl99Bj4MXBPezqN82/W2LDaxj7gWPP/V0t+c22i4m+JvpeexVrGqftLkYKYR7CyrECZq91EYq0Zb6jDLvkCGZYo3t4d9Ge/dGpCN4nsQZR8Q6L2vCDh95vGW+C8m99/zXlYlqUBIXbcIkn5RwbA9ysu0EUIZSUaN10qUjexv1DU2DjFrNziXB7QqmJzQIRkhp5MsmqnTVzIfuJZhN+S+3gBVyjKVEltjw4NSUq6QtPpySsMfYivOZhQXJUi9Akk1fs4vHnEW21jXj/hLJF4SOUqEFrDnLbVQszJ+/u/SCAqS61B9HAciM+qEz9BZPtPZdxyi7RQpfPc2PGgn6jwrj+9EBXuNBdBJxfwIORwb+bYt6TgYVBxY+C1IIwXDHIplf49jXqvs6/Ud/Z7fROXuyEqVZhQx8401of2xj0ykFXeqF0fjQYt7rhZz8W+2M=
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDArp5sWCUkqQqOtcCwtsgNaZdxFtjo+X59ccc4DQk7UuCVKSo+XpBxAzGJTGois/PKhcS0IeEqZ8IY7i1SsG0B2JsOt1f7o/R2231C3YQiAndKTg2035VhxCLNd5/6TeqEYl0RDYwmvkFAHRFEwY+nXgppTIV61+t6kG2lxQr37ay0vxrMA4ytN8G9mTKxnkCDmbWPBzwtQ8HRCiK/iJ7lSLedjs5hmsOeD6DqxXRuXJjkSdXaSNFIfo6lGYQ+7x1YPHt4ODL54Aba9JDNrZZkLMS2fRorwqZARDlvXM7d3pbryVdTKyyF8wO8DKnDvSUfhUk3Kt2R64TPIFUN8Kh4YxP41Lf5Z/BsSZDm4RGoTylAIdtNte6JzhlHr1Ye7nS4M3UEGZB/jzxwAn0uP0AVMJK4D9hBydw/0tQEWWfVIzDjj20m8Qllcuvo8Yi7da1Jv2ds1Wv6Zh7E+MLiuKiELV0iYHs5z43Vly8wb7DCv60kejHJO5ST4M8pLFvmr6k=
       - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKJ8+9KdtKvyipljH+5btS0OLTSRoZKhWKamOnq5+ebi
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKHpntLfKSPaIhORrifoUj8KUhG0MwR6cF08TRdFjbQgV8k+QCZGeLxVJGhIyTKqM/dD2hxvF5dneGYSkSaWG/5HB3n10qF7y3zB55RufSRi5zCQzsJ4cxh8X3mMWjMPUVFQQpndtY/nOFiytmPbAKcPSP4VmJ4NpYqXfVvDWdFU279yroGpFSp9h4r5p9KLPlkOyk+5iFIV8u33AeALPoyGb3P1I5fSUTzpbYy58jSc1tyLJ3knfsckNPbQHd6sW7iwFsWVtgR0CdKVNwoJW6DkfxTHwSZukTRe595FOCGl4EW4VuJXD5sSwy23RcenJU57/s+gL2kBQXQNu08JINqKTjUDXw7PtoQaqR7Sb7ztyrSJbGsLq788cQ3DF55esArOCGAGN8wF/YQfYFMKLbhOpMJo8EUNOZ0Xl1yqBWapSieaeAKertkRyC7+hqoGOzE2a+s5ioQsjfzwFfTm2D4r9V2/VdLyc6yMoBdGnmE/9+OSiRPJMxpTRE92tkQn8=
       - sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBIs5BzhR6ERMfaFX7hFEtT2oYsG9Dm1ZNhjPTQotio27OZgXBzICNGm0/asglYEP2/4z/+kb1l7UZjEQUi+2vbkAAAAEc3NoOg==

chpasswd:
  expire: False
  users:
  - name: $CLOUD_INIT_USER
    password: arg
  - name: root
    password: arg

# Network Configuration (Netplan)
network:
  version: 2
  renderer: networkd
  ethernets:
    $ETHERNET_INTERFACE:
      addresses:
        - $IP_ADDRESS
      nameservers:
        addresses:
          - $GATEWAY
      routes:
        - to: 0.0.0.0/24
          via: $GATEWAY

# Cloud-init logs go here (/var/log/cloud-init.log = default)
output:
  all: "| tee -a /var/log/cloud-init-output.log"

# Enable modules that run on first boot
cloud_init_modules:
  - migrator
  - seed_random
  - bootcmd
  - write-files
  - growpart
  - resizefs
  - set_hostname
  - update_hostname
  - update_etc_hosts
  - rsyslog
  - users-groups
  - ssh

# Enable modules that run on every boot
cloud_config_modules:
  - mounts
  - locale
  - set-passwords
  - package-update-upgrade-install
  - apt-configure
  - ntp
  - timezone
  - runcmd

# Enable modules that run once per instance
cloud_final_modules:
  - scripts-per-once
  - scripts-per-boot
  - scripts-per-instance
  - scripts-user
  - ssh-authkey-fingerprints
  - keys-to-console
  - phone-home
  - final-message
  - power-state-change

# System and/or distro specific settings
system_info:
  default_user:
    name: $CLOUD_INIT_USER
    lock_passwd: false
    gecos: Ubuntu
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    shell: /bin/bash
  # Other config here
EOF

figlet "Cleaning Up"
# Unmount the bind mounts
sudo umount "$ROOT_MOUNT/boot/efi"
sudo umount "$ROOT_MOUNT/dev"
sudo umount "$ROOT_MOUNT/proc"
sudo umount "$ROOT_MOUNT/sys"

# Unmount the partitions and detach the loop device
sudo umount "$EFI_MOUNT"
sudo umount "$ROOT_MOUNT"
sudo losetup -d "${LOOPDEV}"

figlet "Done"
# Notify that the image is ready
echo "EFI image creation complete with netplan and cloud-init. The image is available as $IMAGE_NAME."
