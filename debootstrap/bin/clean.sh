#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

IMAGE_FILE=${PWD}/../build/image.raw
IMAGE_MNT=${PWD}/../build/image.mnt
IMAGE_CONFIG=${PWD}/../build/config.yml

sudo umount ${IMAGE_MNT}/boot/efi || echo "not mounted"
sudo umount -R ${IMAGE_MNT}/ || echo "not mounted"
rm -rf ${IMAGE_FILE} ${IMAGE_MNT} ${IMAGE_CONFIG}
sudo losetup --detach-all
sudo losetup --json
sudo losetup --json | jq '.loopdevices | length'
