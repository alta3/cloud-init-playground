#!/bin/bash
set -euxo pipefail

# Path to the image file
IMAGE_FILE="efi_image.img"

# Check if the image file exists
if [ ! -f "$IMAGE_FILE" ]; then
  echo "Error: Image file $IMAGE_FILE not found!"
  exit 1
fi

# List available disks and prompt user to select the USB drive
echo "Available disk devices:"
lsblk

echo ""
read -p "Enter the device path for the USB drive (e.g., /dev/sdX): " USB_DRIVE

# Confirm the selected device
read -p "Are you sure you want to write $IMAGE_FILE to $USB_DRIVE? This will erase all data on $USB_DRIVE! (y/n): " CONFIRM

if [[ "$CONFIRM" != "y" ]]; then
  echo "Operation canceled."
  exit 1
fi

# Ensure the USB drive is unmounted
echo "Unmounting all partitions on $USB_DRIVE..."
sudo umount "${USB_DRIVE}"* || true

# Write the image to the USB drive
echo "Writing $IMAGE_FILE to $USB_DRIVE..."
sudo dd if="$IMAGE_FILE" of="$USB_DRIVE" bs=4M status=progress

# Flush the write cache to ensure all data is written
echo "Flushing write cache..."
sudo sync

# Notify the user of completion
echo "Image successfully written to $USB_DRIVE."
echo "You can now safely eject the USB drive and boot from it."

exit 0

