#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

mkdir -p ${HOME}/.local/bin/
curl \
    --location \
    --silent \
    https://github.com/hairyhenderson/gomplate/releases/download/v3.11.7/gomplate_linux-amd64 \
    --output ${HOME}/.local/bin/gomplate
chmod +x ${HOME}/.local/bin/gomplate

sudo apt update
sudo apt install -y \
        qemu-utils \
        debootstrap \
        arch-install-scripts \
	dosfstools \
	curl \
        jq \
	pv
        #qemu-system \
