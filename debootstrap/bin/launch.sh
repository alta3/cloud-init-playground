#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

RELEASE=bookworm
IMAGE_QCOW2=${PWD}/../debian-${RELEASE}-remotecloud.qcow2 
VM_ID=$(cat /proc/sys/kernel/random/uuid | cut -d "-" -f 1)
VM_DIR=${PWD}/../vm
VM_IMAGE=${VM_DIR}/${VM_ID}.qcow2

mkdir -p ${VM_DIR}
cp ${IMAGE_QCOW2} ${VM_IMAGE}
qemu-img resize ${VM_IMAGE} 16G

sudo qemu-system-x86_64 \
        -net nic \
        -net user \
        -machine accel=kvm:tcg \
        -enable-kvm \
        -cpu host \
        -m 2048 \
        -nographic \
        -chardev stdio,id=char0,mux=on,logfile=${VM_DIR}/${VM_ID}.serial.log,signal=off \
        -serial chardev:char0 -mon chardev=char0 \
        -drive file=${VM_IMAGE}
