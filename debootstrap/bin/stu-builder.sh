#!/bin/bash
set -euxo pipefail

# Define this server's hostname
HOSTNAME="router"
NAMESERVER="192.168.200.1"  # Replace with the desired nameserver IP address

# Specify a Debian mirror to use
MIRROR="http://ftp.us.debian.org/debian/"

# Create an empty disk image
dd if=/dev/zero of=efi_image.img bs=1M count=3072  # 3GB image size to accommodate the root filesystem and EFI partition

# Create a GPT partition table and two partitions: EFI and root
parted efi_image.img --script -- mklabel gpt
parted efi_image.img --script -- mkpart ESP fat32 1M 512M
parted efi_image.img --script -- mkpart primary ext4 512M 100%

# Set the boot flag on the EFI partition
parted efi_image.img --script -- set 1 boot on

# Associate the image with a loop device
LOOPDEV=$(sudo losetup -fP --show efi_image.img)

# Format the partitions
sudo mkfs.vfat -F 32 "${LOOPDEV}p1"
sudo mkfs.ext4 "${LOOPDEV}p2"

# Mount the partitions
EFI_MOUNT="$HOME/efi-mount"
ROOT_MOUNT="$HOME/root-mount"
sudo mkdir -p "$EFI_MOUNT"
sudo mount "${LOOPDEV}p1" "$EFI_MOUNT"

sudo mkdir -p "$ROOT_MOUNT"
sudo mount "${LOOPDEV}p2" "$ROOT_MOUNT"

# Install the base Debian system using debootstrap
sudo debootstrap --arch=amd64 bookworm "$ROOT_MOUNT" "$MIRROR"

# Ensure /boot/efi exists and mount the EFI partition inside chroot
# Hey Dawg, I heard you like mountpoints, so I put a mountpoint inside 
# your mountpoints, so you can mount while you are chrooting.
# Seems like there should be a better way, but this overcame the chroot limitation.
sudo mkdir -p "$ROOT_MOUNT/boot/efi"
sudo mount --bind /dev "$ROOT_MOUNT/dev"
sudo mount --bind /proc "$ROOT_MOUNT/proc"
sudo mount --bind /sys "$ROOT_MOUNT/sys"
sudo mount "${LOOPDEV}p1" "$ROOT_MOUNT/boot/efi"

# Update package list and install required packages with apt
sudo chroot "$ROOT_MOUNT" apt-get update
sudo chroot "$ROOT_MOUNT" apt-get install -y sudo vim curl openssh-server git tmux qemu-utils gdisk jq pv  linux-image-amd64 netplan.io cloud-init grub-efi-amd64 locales tzdata


# Configure locale
sudo chroot "$ROOT_MOUNT" bash -c 'echo "en_US.UTF-8 UTF-8" > /etc/locale.gen'
sudo chroot "$ROOT_MOUNT" locale-gen
sudo chroot "$ROOT_MOUNT" bash -c 'echo "LANG=en_US.UTF-8" > /etc/default/locale'

# Set timezone
sudo chroot "$ROOT_MOUNT" ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
sudo chroot "$ROOT_MOUNT" dpkg-reconfigure -f noninteractive tzdata

# Configure keyboard layout
sudo tee "$ROOT_MOUNT/etc/default/keyboard" > /dev/nul <<EOF
# KEYBOARD CONFIGURATION FILE
XKBMODEL="pc105"
XKBLAYOUT="us"
XKBVARIANT=""
XKBOPTIONS=""
BACKSPACE="guess"
EOF'

# Manually configure any problematic packages
sudo chroot "$ROOT_MOUNT" dpkg --configure -a
sudo chroot "$ROOT_MOUNT" apt-get install -f

# Ensure the initramfs is generated
sudo chroot "$ROOT_MOUNT" update-initramfs -u

# Set the root password 
sudo chroot "$ROOT_MOUNT" bash -c "echo 'root:arg' | chpasswd"
# sudo chroot "$ROOT_MOUNT" bash -c "usermod --password 'hash' root"
# apt install whois
# PASSWORD_HASH=$(mkpasswd --method=SHA-512 --rounds=5000 'yourpassword')

# Looks like an artifact of initial testing, should be removed.
# Create a new user with sudo privileges
sudo chroot "$ROOT_MOUNT" bash -c "useradd -m -s /bin/bash ubuntu"
sudo chroot "$ROOT_MOUNT" bash -c "echo 'ubuntu:arg' | chpasswd"
sudo chroot "$ROOT_MOUNT" bash -c "adduser ubuntu sudo"

# No netword manager is running here, so DNS must be set manually
sudo tee "$ROOT_MOUNT/etc/resolv.conf" > /dev/null <<EOF
nameserver $NAMESERVER
options edns0 trust-ad
search alta3.com
EOF

# TODO: Fix this so it doesn't 'no such file or directory' Disable/Remove Language Warning
# sudo tee "$ROOT_MOUNT/var/lib/cloud/instances/iid-datasource-none/locale-check.skip" > /dev/null <<EOF
# Disabling Language Warning
# EOF
# figlet "Disable Language"


# Hmmmm, networkd will not start without this in place
# Cloud-init trying to run ifup, so this cloud-init is not thinking networkd
sudo tee "$ROOT_MOUNT/etc/netplan/01-netcfg.yaml" > /dev/null <<EOF
network:
  version: 2
  ethernets:
    enp2s0:
      addresses:
        - 192.168.200.31/24
      nameservers:
        addresses:
          - 192.168.200.1
      routes:
        - to: 0.0.0.0/0
          via: 192.168.200.1
EOF

# netplan shields up
sudo chmod 600 "$ROOT_MOUNT/etc/netplan/01-netcfg.yaml"


# Install GRUB to the EFI partition
sudo chroot "$ROOT_MOUNT" grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB --removable --recheck --no-floppy

# Dynamically detect the kernel and initramfs versions and configure GRUB with /dev/sdb2
KERNEL_VERSION=$(basename "$ROOT_MOUNT/boot/vmlinuz-"* | sed 's/vmlinuz-//')
INITRD_VERSION=$(basename "$ROOT_MOUNT/boot/initrd.img-"* | sed 's/initrd.img-//')


# Write the GRUB configuration with /dev/sdb2
sudo tee "$ROOT_MOUNT/boot/grub/grub.cfg" > /dev/null <<EOF
set timeout=5
set default=0

menuentry "Alta3 Custom Debian" {
    set root=(hd0,gpt2)
    linux /boot/vmlinuz-$KERNEL_VERSION root=/dev/sdb2 ro
    initrd /boot/initrd.img-$INITRD_VERSION
}
EOF


# Hard code /etc/fstab with /dev/sdb2
# TODO: remotecloud knows the disk ID.
sudo tee "$ROOT_MOUNT/etc/fstab" > /dev/null <<EOF
/dev/sdb2 / ext4 errors=remount-ro 0 1
EOF

# MOTD 
sudo tee "$ROOT_MOUNT/etc/motd" > /dev/null <<EOF

RRRRR      oOo   U    U  TTTTTTT  EEEEEE  RRRRR
R    R    O   O  U    U     T     E       R    R
RRRRR     O   O  U    U     T     EeE     RRRRR
R  R      O   O  U    U     T     Ee      R  R 
R   R     O   O  U    U     T     E       R   R
R    R     OOO    uuuu      T     EEEEE   R    R

EOF


# Set DNS must be set manually (no network manager)
sudo tee "$ROOT_MOUNT/etc/resolv.conf" > /dev/null <<EOF
nameserver $NAMESERVER
options edns0 trust-ad
search alta3.com
EOF

# BEGIN CLOUD-INIT STUFF
#######################################################################
# Spent most of my time here, so I am adding some notes that I want    #
# others to know and not learn the hard way.                           #
# 0. Debugging a cloud-init is straightforward if your know the        #
#    following things to check:                                        #
# 1. cloud-init is a systemd svc (sudo systemctl status cloud-init)    #
#    that does most of its work only once, at the first boot.          #
# 2. When debugging on the target host, this will trigger cloud-init   #
#    service to try again even though it already initialized           #
#      sudo cloud-init clean                                           #
#      sudo cloud-init init                                            #
#      sudo cloud-init modules --mode=config                           #
#      sudo cloud-init modules --mode=final                            #
# 3. During installation debugging, I got a warning that in the future #
#    cloud-init will only attempt to use datasources that are          #
#    identified or specifically configured. (I guess the timeout was   #
#    judged as excessive by complaining users, while cloud-int         #
#    searches many network sources with multi-minute timeouts, so the  #
#    dev staff decided to speed things up with  a configuration rule   #
#    that must be part of /etc/cloud/cloud.cfg (what I did), or place  #
#    two-line yaml in its own file like this:                          #
#      sudo tee /etc/cloud/cloud.cfg.d/90_dpkg.cfg > /dev/null <<EOF   #
#      #cloud-config                                                   #
#      datasource_list: [ NoCloud, None ]                              #
#      EOF                                                             #
# 4. In testing, "datasource_list: [ None ]" Looks best, here is why:  #
#    cloud-init is designed to search for config data via IP, but in   #
#    our case, it will find nothing except a multi-minute timeout as   #
#    I have no network available data sources. "None" causes           #
#    cloud-init to skip the network step and read immediately from     #
#    /etc/cloud/cloud.cfg and *.cfg file fragements in                 #
#    /etc/cloud/cloud.cfg.d/. So by chosing "None" the network timeout #
#    is eliminated which just raises another question, why is "None"   #
#    called "None" when clearly checking the /etc/cloud/cloud.cfg is a #
#    very big "Something". Even more perplexing is that after a multi- #
#    minute timeout, datasource "NoCloud" will look for files on the   #
#    local drive, inside /var/lib/cloud/seed/nocloud, for the classic  #
#    user-data, metadata, and network-cfg files. So both methods can   #
#    read local files, but NoCloud will do so after a timeout.         #
#    Data-source None means check only system level files, which can   #
#    contain data otherwise obtained by outside data sources.          #
# 5. Cloud-init's NoCloud directory = /var/lib/cloud/ (FYI)            #
# 6  Cloud-init's "None" directory = /etc/cloud/cloud.cfg              #
# 7. Cloud-init data-source can also be set by grub's kernel cmd when  #
#    starting linux. More on that in the next iteration                #
########################################################################

# Configure cloud-init
sudo tee "$ROOT_MOUNT/etc/cloud/cloud.cfg" > /dev/null <<EOF
#cloud-config

# Specify the datasource
datasource_list: [ None ]

#Stop cloud-init from overwriting /etc/apt/sources.list
apt_preserve_sources_list: true

disable_root: false

hostname: router

users:
  - name: ubuntu
    lock_passwd: false
    gecos: Ubuntu
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    shell: /bin/bash
    # mkpasswd --method=SHA-512 --rounds=5000 'yourpassword')
    ssh_authorized_keys:
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDSSpuNstkhwIjQlChmVymOnwJ9gBKwV/D4odIDoz3UiFoHYcq5uoz5jk9RyUQAirT0jmtDrPgiBzw7cKk54q8h1x40uvH+GqChcqOXYMUsPnIONOYYOxn/R6mqvzsmtlMs5865f4KgFzNmjyN9YJoNh3x3mELnCBSDBqWyc+nKSBmFBQmBW/mPA9yGp9zQ90AnznjyjqKS9WQlRO/9Bv/iJLoR8AAOgC5WdxXZu6Vm8IVZIhPu3w+cuumPuYhu9p2xDJrQERyk1QOVKbNF1JtfAUQYQNOnq4ZiC8NW+f4kvY/ugqreoHI9zsUP0ddTQk7WtfOSe/TWjYMXkCc6TunKmr4Sq9Qh8cU7laq1fBaGoRgu+tzqSsZ0eyqP/Y/sCcDlfVPPbU9otI5rGzkVjrXLbO9OuH4bKQFgnyDV2wYZbiGyipXor87YLN5U5KwJxNz1zNAbJ3bGfYZQ5eLXXBjgl2ErLaseBlmZspYo49Ogn0AHosTuQ4b+sJoVU6WpUms=
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCZZ+/2c4E0iIykECZwlEeaDraUzw3eSzlnICfXcx4rxMaKGCQu4FoEq6b7DToUvJxWGATl99Bj4MXBPezqN82/W2LDaxj7gWPP/V0t+c22i4m+JvpeexVrGqftLkYKYR7CyrECZq91EYq0Zb6jDLvkCGZYo3t4d9Ge/dGpCN4nsQZR8Q6L2vCDh95vGW+C8m99/zXlYlqUBIXbcIkn5RwbA9ysu0EUIZSUaN10qUjexv1DU2DjFrNziXB7QqmJzQIRkhp5MsmqnTVzIfuJZhN+S+3gBVyjKVEltjw4NSUq6QtPpySsMfYivOZhQXJUi9Akk1fs4vHnEW21jXj/hLJF4SOUqEFrDnLbVQszJ+/u/SCAqS61B9HAciM+qEz9BZPtPZdxyi7RQpfPc2PGgn6jwrj+9EBXuNBdBJxfwIORwb+bYt6TgYVBxY+C1IIwXDHIplf49jXqvs6/Ud/Z7fROXuyEqVZhQx8401of2xj0ykFXeqF0fjQYt7rhZz8W+2M=
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDArp5sWCUkqQqOtcCwtsgNaZdxFtjo+X59ccc4DQk7UuCVKSo+XpBxAzGJTGois/PKhcS0IeEqZ8IY7i1SsG0B2JsOt1f7o/R2231C3YQiAndKTg2035VhxCLNd5/6TeqEYl0RDYwmvkFAHRFEwY+nXgppTIV61+t6kG2lxQr37ay0vxrMA4ytN8G9mTKxnkCDmbWPBzwtQ8HRCiK/iJ7lSLedjs5hmsOeD6DqxXRuXJjkSdXaSNFIfo6lGYQ+7x1YPHt4ODL54Aba9JDNrZZkLMS2fRorwqZARDlvXM7d3pbryVdTKyyF8wO8DKnDvSUfhUk3Kt2R64TPIFUN8Kh4YxP41Lf5Z/BsSZDm4RGoTylAIdtNte6JzhlHr1Ye7nS4M3UEGZB/jzxwAn0uP0AVMJK4D9hBydw/0tQEWWfVIzDjj20m8Qllcuvo8Yi7da1Jv2ds1Wv6Zh7E+MLiuKiELV0iYHs5z43Vly8wb7DCv60kejHJO5ST4M8pLFvmr6k=
       - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKJ8+9KdtKvyipljH+5btS0OLTSRoZKhWKamOnq5+ebi
       - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDKHpntLfKSPaIhORrifoUj8KUhG0MwR6cF08TRdFjbQgV8k+QCZGeLxVJGhIyTKqM/dD2hxvF5dneGYSkSaWG/5HB3n10qF7y3zB55RufSRi5zCQzsJ4cxh8X3mMWjMPUVFQQpndtY/nOFiytmPbAKcPSP4VmJ4NpYqXfVvDWdFU279yroGpFSp9h4r5p9KLPlkOyk+5iFIV8u33AeALPoyGb3P1I5fSUTzpbYy58jSc1tyLJ3knfsckNPbQHd6sW7iwFsWVtgR0CdKVNwoJW6DkfxTHwSZukTRe595FOCGl4EW4VuJXD5sSwy23RcenJU57/s+gL2kBQXQNu08JINqKTjUDXw7PtoQaqR7Sb7ztyrSJbGsLq788cQ3DF55esArOCGAGN8wF/YQfYFMKLbhOpMJo8EUNOZ0Xl1yqBWapSieaeAKertkRyC7+hqoGOzE2a+s5ioQsjfzwFfTm2D4r9V2/VdLyc6yMoBdGnmE/9+OSiRPJMxpTRE92tkQn8=
       - sk-ecdsa-sha2-nistp256@openssh.com AAAAInNrLWVjZHNhLXNoYTItbmlzdHAyNTZAb3BlbnNzaC5jb20AAAAIbmlzdHAyNTYAAABBBIs5BzhR6ERMfaFX7hFEtT2oYsG9Dm1ZNhjPTQotio27OZgXBzICNGm0/asglYEP2/4z/+kb1l7UZjEQUi+2vbkAAAAEc3NoOg==

# Network Configuration (Netplan)
network:
  version: 2
  renderer: networkd
  ethernets:
    enp2s0:
      addresses:
        - 192.168.200.31/24
      nameservers:
        addresses:
          - 192.168.200.1
      routes:
        - to: 0.0.0.0/24
          via: 192.168.200.1

# Cloud-init logs go here (/var/log/cloud-init.log = default)
output:
  all: "| tee -a /var/log/cloud-init-output.log"

# Enable modules that run on first boot
cloud_init_modules:
  - migrator
  - seed_random
  - bootcmd
  - write-files
  - growpart
  - resizefs
  - set_hostname
  - update_hostname
  - update_etc_hosts
  - rsyslog
  - users-groups
  - ssh

# Enable modules that run on every boot
cloud_config_modules:
  - mounts
  - locale
  - set-passwords
  - package-update-upgrade-install
  - apt-configure
  - ntp
  - timezone
  - runcmd

# Enable modules that run once per instance
cloud_final_modules:
  - scripts-per-once
  - scripts-per-boot
  - scripts-per-instance
  - scripts-user
  - ssh-authkey-fingerprints
  - keys-to-console
  - phone-home
  - final-message
  - power-state-change

# System and/or distro specific settings
system_info:
  default_user:
    name: debian
    lock_passwd: false
    gecos: Debian
    sudo: ["ALL=(ALL) NOPASSWD:ALL"]
    shell: /bin/bash
  # Other config here
EOF

# Unmount the bind mounts
sudo umount "$ROOT_MOUNT/boot/efi"
sudo umount "$ROOT_MOUNT/dev"
sudo umount "$ROOT_MOUNT/proc"
sudo umount "$ROOT_MOUNT/sys"

# Unmount the partitions and detach the loop device
sudo umount "$EFI_MOUNT"
sudo umount "$ROOT_MOUNT"
sudo losetup -d "${LOOPDEV}"

# Notify that the image is ready
echo "EFI image creation complete with netplan and cloud-init. The image is available as efi_image.img."
figlet Done

