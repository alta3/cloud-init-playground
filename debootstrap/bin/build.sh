#!/bin/bash
set -euxo pipefail
cd -P -- "$(dirname -- "${BASH_SOURCE[0]}")" && pwd -P

## set variables
GOMPLATE=${HOME}/.local/bin/gomplate
RELEASE=bookworm
NEXT_RELEASE=trixie
MIRROR=http://debian.csail.mit.edu/debian/
DRIVE_SIZE=3221225472 ## 3GB

IMAGE_FILE=${PWD}/../build/image.raw
IMAGE_MNT=${PWD}/../build/image.mnt
IMAGE_CONFIG=${PWD}/../build/config.yml
IMAGE_QCOW2=${PWD}/../artifacts/debian-${RELEASE}-remotecloud.qcow2 
IMAGE_RAW=${PWD}/../artifacts/debian-${RELEASE}-remotecloud.raw

FILES_DIR=../config/files
TEMPLATES_DIR=../config/templates

## cleanup at start - always fresh
./clean.sh

## Create image file and parition
mkdir -p ${IMAGE_MNT}
dd if=/dev/zero of=${IMAGE_FILE} bs=1 count=0 seek=${DRIVE_SIZE} status=progress
## GPT partition table
# 1  - system 8300 linux
# 14 -        ef02 BIOS
# 15 -        ef00 EFI
sudo sgdisk ${IMAGE_FILE} \
	--clear \
	--new 14:2048:+3M \
	--typecode 14:ef02 \
	--new 15:0:+124M \
	--typecode 15:ef00 \
	--new 1:0: \
	--typecode 1:8300 \
	--print

## mount & format disk partition
sudo losetup --find --partscan ${IMAGE_FILE}
sudo losetup --json
sleep 0.5 # give  it a second
sudo mkfs.ext4 /dev/loop0p1
sudo mount /dev/loop0p1 ${IMAGE_MNT}
sudo mkdir -p ${IMAGE_MNT}/boot/efi
sudo mkfs.fat -F 32 /dev/loop0p15
sudo mount /dev/loop0p15 ${IMAGE_MNT}/boot/efi

## install system
sudo debootstrap \
 	--arch=amd64 \
 	--include=ca-certificates \
 	${RELEASE} \
 	${IMAGE_MNT} \
 	${MIRROR} 

## template and write configurations
cat << EOF > ${IMAGE_CONFIG}
---
release: ${RELEASE}
next_release: ${NEXT_RELEASE}
mirror: ${MIRROR}
nocloud_source: "http://remotecloud.trustme.click/imds/__dmi.system-uuid__/"
EOF

${GOMPLATE} --out - \
	    --context .=${IMAGE_CONFIG} \
	    --file ${TEMPLATES_DIR}/sources.list.tmpl \
	    | sudo tee ${IMAGE_MNT}/etc/apt/sources.list 
${GOMPLATE} --out - \
	    --context .=${IMAGE_CONFIG} \
            --file ${TEMPLATES_DIR}/testing-packages.tmpl \
	    | sudo tee ${IMAGE_MNT}/etc/apt/preferences.d/${NEXT_RELEASE}-packages
${GOMPLATE} --out - \
	    --context .=${IMAGE_CONFIG} \
	    --file ${TEMPLATES_DIR}/stable-packages.tmpl \
	    | sudo tee ${IMAGE_MNT}/etc/apt/preferences.d/${RELEASE}-packages
${GOMPLATE} --out - \
	    --context .=${IMAGE_CONFIG} \
	    --file ${TEMPLATES_DIR}/default.grub.tmpl \
	    | sudo tee ${IMAGE_MNT}/etc/default/grub

sudo cp ${FILES_DIR}/ignored-packages \
	${IMAGE_MNT}/etc/apt/preferences.d/ignored-packages
sudo cp ${FILES_DIR}/chroot-install.sh \
	${IMAGE_MNT}/chroot-install.sh

genfstab -U ${IMAGE_MNT} | grep -v -E "^#|^$|swap" | sudo tee ${IMAGE_MNT}/etc/fstab

## compelte install within chroot
sudo arch-chroot ${IMAGE_MNT} /bin/bash /chroot-install.sh 
sudo rm ${IMAGE_MNT}/chroot-install.sh

## unmount
sudo umount -R ${IMAGE_MNT}/     || echo "not mounted"
rm -rf ${IMAGE_MNT} ${IMAGE_CONFIG}
sudo losetup --detach-all

## convert to qcow2 and save raw output
qemu-img convert \
	-f raw ${IMAGE_FILE} \
	-O qcow2 ${IMAGE_QCOW2}
cp ${IMAGE_FILE} ${IMAGE_RAW}

## cleanup 
./clean.sh
