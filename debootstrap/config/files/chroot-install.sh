#!/bin/bash
set -eux

apt update

DEBIAN_FRONTEND=noninteractive \
/usr/bin/apt-get install -y \
	linux-image-generic \
	linux-headers-generic \
	firmware-linux \
	grub-efi-amd64 \
	cloud-init \
	netplan.io \
	openssh-server \
	lshw \
	sudo \
	zstd \
	man \
	vim \
	git \
	tmux \
	htop \
	curl

grub-install \
	--verbose \
	--recheck \
	--target=x86_64-efi \
	--force-extra-removable \
	/dev/loop0 
update-initramfs -u
update-grub

/usr/bin/apt-get autoremove -y
